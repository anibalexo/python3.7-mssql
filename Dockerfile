FROM python:3.7

MAINTAINER Anibal Gonzalez <aigonzalez@lojagas.com>

# set environment variables python
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# apt-get and system utilities for pyodbc
RUN apt-get update && apt-get install -y --no-install-recommends \
    gcc \
    g++ \
    unixodbc-dev \
    unixodbc \
    libpq-dev \
    tdsodbc

RUN apt-get install -y freetds-common freetds-bin freetds-dev
ADD odbcinst.ini /etc/
RUN odbcinst -i -d -f /usr/share/tdsodbc/odbcinst.ini

# set config file Freetds
COPY freetds.conf /etc/freetds/freetds.conf

